﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Controller2D))]
public class Player : MonoBehaviour
{

    public float maxJumpHeight = 4;
    public float minJumpHeight = 1;
    public float timeToJumpApex = .4f;
    float accelerationTimeAirborne = .2f;
    float accelerationTimeGrounded = .1f;
    public float moveSpeed;
    public float sprintMoveSpeed;

    public Vector2 wallJumpClimb;
    public Vector2 wallJumpOff;
    public Vector2 wallLeap;

    [HideInInspector]
    public float playerHealth = 1000;
    public Vector2 velocity;

    public float wallSlideSpeedMax = 3;
    public float wallStickTime = .25f;
    float timeToWallUnstick;

    float gravity;
    float maxJumpVelocity;
    float minJumpVelocity;
    [Range(.25f, 5)]
    public float speed;
    float velocityXSmoothing;
    public bool sprint;

    Controller2D controller;
    CameraFollow cam;
    Player player;

    Vector2 directionInput;
    bool wallSliding;
    int wallDirX;

    public Vector2 StartLocation;

    void Start()
    {
        controller = GetComponent<Controller2D>();
        cam = GetComponent<CameraFollow>();
        player = this;

        StartLocation = player.transform.position;
        gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
    }

    void Update()
    {
        CalculateVelocity();
        HandleWallSliding();

        controller.Move(velocity * Time.deltaTime, directionInput);

        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
        }
		/*
        if(this.transform.position.x >=  (cam.maxX + cam.focusAreaSize.x)){
            print("Here");
            OnDeath();
        }*/
    }

    public void SetDirectionalInput(Vector2 input)
    {
        directionInput = input;
    }

    public void OnJumpInputDown()
    {
        if (wallSliding)
        {
            if (wallDirX == directionInput.x)
            {
                velocity.x = -wallDirX * wallJumpClimb.x;
                velocity.y = wallJumpClimb.y;
            }
            else if (directionInput.x == 0)
            {
                velocity.x = -wallDirX * wallJumpOff.x;
                velocity.y = wallJumpOff.y;
            }
            else
            {
                velocity.x = -wallDirX * wallLeap.x;
                velocity.y = wallLeap.y;
            }
        }
        if (controller.collisions.below)
        {
            velocity.y = maxJumpVelocity;
        }
    }

    public void OnJumpInputUp()
    {
        if (velocity.y > minJumpVelocity)
        {
            velocity.y = minJumpVelocity;
        }
    }



    void HandleWallSliding()
    {
        wallDirX = (controller.collisions.left) ? -1 : 1;
        wallSliding = false;
        if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below)
        {
            wallSliding = true;

            
            if (velocity.y < -wallSlideSpeedMax)
            {
                velocity.y = -wallSlideSpeedMax;
            }
            if (timeToWallUnstick > 0 )
            {
                velocityXSmoothing = 0;
                if (velocity.y < 0) {
                    velocity.x = 0;
                }
                if (directionInput.x != wallDirX && directionInput.x != 0)
                {
                     timeToWallUnstick -= Time.deltaTime;
                }
                else
                {
                    timeToWallUnstick = wallStickTime;
                }
            }
            else
            {
                timeToWallUnstick = wallStickTime;
                }
        }

    }

    void CalculateVelocity()
    {
        float targetVelocityX;
        if (PlayerPrefs.GetInt("Sprint") >= 1)
        {
            targetVelocityX = directionInput.x * sprintMoveSpeed;
        }
        else
        {
            targetVelocityX = directionInput.x * moveSpeed;
        }
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        velocity.y += gravity * Time.deltaTime;
    }
}