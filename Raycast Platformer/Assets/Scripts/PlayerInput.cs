﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Player))]

public class PlayerInput : MonoBehaviour {


    Player player;
    PlayerAttack attack;
    void Start()
    {
        player = GetComponent<Player>();
        /*if (GetComponent<PlayerAttack>())
        {
*/            attack = GetComponent<PlayerAttack>();
        //}
    }
	// Update is called once per frame
	void Update () {
		Vector2 directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        player.SetDirectionalInput(directionalInput);

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetAxisRaw("Sprint") > 0)
        {
            PlayerPrefs.SetInt("Sprint", 1);
        }
        else
        {
            PlayerPrefs.SetInt("Sprint", 0);
        }
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Jump"))
        {
            player.OnJumpInputDown();
        }
        if (Input.GetKeyUp(KeyCode.Space) || Input.GetButtonUp("Jump"))
        {
            player.OnJumpInputUp();
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            attack.Attack();
        }
    }
}
