﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INoiseMaker
{
    void MadeSound();
    float IntensityOfSound { get; }
    Vector3 Position { get; }
}
