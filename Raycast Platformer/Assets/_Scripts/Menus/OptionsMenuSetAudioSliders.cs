﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenuSetAudioSliders : MonoBehaviour {


    public Slider MasterVolume;
    public Slider BackgroundVolume;
    public Slider SFXVolume;

    private void Start()
    {
        if (AudioMaster.instance)
        {
            MasterVolume.value = AudioMaster.instance.MasterVolume;
            BackgroundVolume.value = AudioMaster.instance.RawMusicValue();
            SFXVolume.value = AudioMaster.instance.RawSFXValue();
        }
    }

    private void Update()
    {
        AudioMaster.instance.MasterVolume = MasterVolume.value;
        AudioMaster.instance.MusicVolume = BackgroundVolume.value;
        AudioMaster.instance.SFXVolume = SFXVolume.value;
    }
    // this is also called i naudiomaster start, so this will go largely unused
    public void LoadAudioSettings () {
		if(AudioMaster.instance)
        {
            AudioMaster.instance.LoadAudioPrefs();
        }
	}

}
