﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Buttons : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler
{
    

    public AudioMaster AudioMaster;
    public AudioClip mouseOverSound;
    public AudioClip mouseClickSound;
    private AudioSource source;
    // Update is called once per frame

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }
    public void OnPointerEnter(PointerEventData ped)
    {
        source.clip = mouseOverSound;
        source.volume = AudioMaster.SFXVolume;
        source.Play();
    }

    public void OnPointerDown(PointerEventData ped)
    {
        source.clip = mouseClickSound;
        source.volume = AudioMaster.SFXVolume;
        source.Play();
    }
}
