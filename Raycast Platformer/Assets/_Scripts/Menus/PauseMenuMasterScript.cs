﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuMasterScript : MonoBehaviour {


    public static bool GameIsPaused = false;
    public GameObject PauseMenu;
    public GameObject OptionsMenu;
    public GameObject DeathMenu;
    private bool playerDead = false;
    public GameObject Inventory;

    private void Start()
    {
    }

    protected void Update()
    {
        if (!playerDead && Input.GetButtonDown("Pause"))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        print(Time.timeScale);
        Cursor.lockState = CursorLockMode.Locked;
        AudioMaster.instance.unPauseSong();
        PauseMenu.SetActive(false);
        OptionsMenu.SetActive(false);
        Inventory.SetActive(true);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    public void Pause()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        AudioMaster.instance.PauseSong();
        PauseMenu.SetActive(true);
        //Inventory.SetActive(false);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void ExitMM()
    {
        Time.timeScale = 0f;
        AudioMaster.instance.StopSong();
        Cursor.lockState = CursorLockMode.Confined;
        SceneManager.LoadScene("MainMenu");
    }

    public void ExitDesktop()
    {
        Application.Quit();
    }

    public void ShowDeathScreen()
    {
        playerDead = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        AudioMaster.instance.PauseSong();
        DeathMenu.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }
}
